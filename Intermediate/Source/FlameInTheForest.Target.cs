using UnrealBuildTool;

public class FlameInTheForestTarget : TargetRules
{
	public FlameInTheForestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("FlameInTheForest");
	}
}
