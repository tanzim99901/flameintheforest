Demo video: https://drive.google.com/file/d/1toCbFys06IcujYg0gSRfEmQN6SFu-9rw/view?usp=sharing

A virtual reality game where the player has to follow a flame monster through a forest until it is destroyed, solving some puzzles along the way. 
Implemented in Unreal Engine 4.26, with both OculusVR and SteamVR support.

A build is included in: \Build\FlameInTheForest1.0\

You will need either a SteamVR device (e.g., HTC VIVE), an Oculus Rift, or an Oculus Quest (with Oculus Link) to play the game.